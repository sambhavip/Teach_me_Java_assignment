package powerpackage;

public class PowerFinder {

    public static int toPower(int x, int y) {
        int p = 1;
        for (int i = 0; i<y; i++) {
            p *= x;
        }
        return p;
    }
    public static void main(String [] args){
        int base = 2;
        int power =3;
        System.out.println (power +" power of base "+base +"= "+toPower(base,power));
    }
}
