package powerpackage;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class PowerTest {
    @Test
    public void one_raised_to_one_is_onew() {
        PowerFinder pf= new PowerFinder();
        Assertion.assertEquals("1",pf.toPower(1,1));
       // assert PowerFinder.OF(1,1) == 1;
    }

    @Test
    public void twoRaised_to_one_is_2() {
        PowerFinder pf= new PowerFinder();
        Assertion.assertEquals("2",pf.toPower(2,1));
        //assert powerfinder.OF(2,1) == 2;
    }

    @Test
    public void two_power_2_is_4() {
        PowerFinder pf= new PowerFinder();
        Assertion.assertEquals("4",pf.toPower(2,2));
        //assert powerfinder.OF(2, 2) == 4;
    }

    @Test
    public void power_of_2_and_3_Is_SIX() {
        PowerFinder pf= new PowerFinder();
        Assertion.assertEquals("9",pf.toPower(3,2));
        //assert powerfinder.OF(3, 2) == 3*3;
    }
}
